import React from "react";
import PropTypes from "prop-types";
import {LineChart} from "react-d3";

export default class Graph extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            lineData: [
                { name: "X", values: [{x: 0, y: 0}]},
                { name: "Y", values: [{x: 0, y: 0}]},
                { name: "Z", values: [{x: 0, y: 0}]},
            ],
        };
    }

    static propTypes = {
        data: PropTypes.array,
        index: PropTypes.number.isRequired,
    }

    parse(data, time) {
        if (!data) {
            return this.state.lineData;
        }
        const palm = data.palmVelocity;
        const n = 20;

        const lineData = this.state.lineData.map((series, index) => {
            const values = series.values;
            values.push({x: time, y: palm[index]});
            return {...series, values: values.slice(-n)};
        });

        this.setState({data});
        return lineData;
    }

    render() {
        return (
            <LineChart
                legend={true}
                data={this.parse(this.props.data, this.props.index)}
                width={500}
                height={400}
                viewBoxObject={{x: 0,y: 0,width: 500,height: 400}}
                title="Line Chart"
                yAxisLabel="mm/s"
                xAxisLabel="Elapsed Time (frame id)"
                gridHorizontal={true}
            />
        );
    }
}
