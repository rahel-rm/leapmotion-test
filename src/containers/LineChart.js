import React from "react";
import PropTypes from "prop-types";
import {LineChart as LineChartD3} from "react-d3";

export default class LineChart extends React.Component {
    static propTypes = {
        data: PropTypes.array,
        key: PropTypes.string,
    }

    render() {
        return (
            <LineChartD3
                legend={true}
                colors={this.colors}
                data={this.props.data}
                width={500}
                height={400}
                viewBoxObject={{x: 0, y: 0, width: 500, height: 400}}
                title={this.props.key}
                yAxisLabel="unit"
                xAxisLabel="Elapsed Time (frame id)"
                gridHorizontal={true}
            />
        );
    }

    colors(series) {
        return ["red", "green", "blue"][series];
    }
}
