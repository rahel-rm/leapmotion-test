import React from "react";
import VLine from "./VLine.js";

export default class Collapse extends React.Component {

    constructor(props) {
        super(props);
        this.state = { open: false };
    }

    render() {
        return (
            <div>
                <button onClick={::this.toggle}>
                    {this.props.title}
                </button>
                {this.state.open && <VLine>{this.props.children}</VLine>}
            </div>
        );
    }

    toggle() {
        this.setState({open: !this.state.open});
    }
}
