import React from "react";
import PropTypes from "prop-types";
import plugins from "leapjs-plugins"; // eslint-disable-line no-unused-vars

export default class HandEntryPlugin extends React.Component {
    static propTypes = {
        controller: PropTypes.object,
    };

    constructor(props) {
        super(props);
        this.state = {};
        this.attachHandlers();
    }

    render() {
        return (
            <p>
              HandEntryPlugin: {this.isHandEntered()}
            </p>
        );
    }

    isHandEntered() {
        return this.state.hand;
    }

    attachHandlers() {
        const notifyHandFound = this.notifyHandFound.bind(this);
        const notifyHandLost = this.notifyHandLost.bind(this);
        this.props.controller.on("connect", () => {
            this.props.controller.use("handEntry");

            this.props.controller.on("handFound", notifyHandFound);

            this.props.controller.on("handLost", notifyHandLost);
        });

        this.props.controller.on("disconnect", () => {
            this.props.controller.stopUsing("handEntry");

            this.props.controller.removeListener("handFound", notifyHandFound);
            this.props.controller.removeListener("handLost", notifyHandLost);
        });
    }

    notifyHandFound(hand) {
        this.setState({hand: `oh yeah (${hand.confidence})`});
    }

    notifyHandLost() {
        this.setState({hand: "put it back"});
    }
}
