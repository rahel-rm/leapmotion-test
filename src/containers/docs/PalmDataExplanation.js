import React from "react";
import prettify from "../../util/prettify.js";

export default class PalmDataExplanation extends React.Component {
    render() {
        return (
            <div>
                <h3>Hand keys, <a href="https://developer.leapmotion.com/documentation/javascript/api/Leap.Hand.html">LINK</a></h3>
                <pre>{this.explainData()}</pre>
            </div>
        );
    }

    explainData() {
        return prettify([
            "id_: Frame ID",
            "arm: Wrist location",
            "confidence: 1 is good, 0 is very distorted",
            "direction: Palm to fingers, rad",
            "fingers: Fingers",
            "grabStrength: 0 is open, 1 is fist",
            "palmPosition: Above leapMotion, mm",
            "palmNormal: Normal from palm to down, rad",
            "palmVelocity: Speed, mm/s",
            "pinchStrength: 0 is open, 1 is thumb in contact with other finger of same hand",
            "pointables: Fingers",
            "sphereCenter: The center of a sphere fit to the curvature of this hand.",
            "sphereRadius: The radius of a sphere fit to the curvature of this hand, in millimeters.",
            "stabilizedPalmPosition: Smoothed palm position",
            "timeVisible: The amount of time this hand has been continuously visible to the Leap Motion controller in seconds.",
            "type: Left or right",
            "tools: // DEPRECATED, empty since 2.0",
            "valid: is it a valid object",
            " _scaleFactor, _translation, _rotation, frame, thumb, indexFinger, middleFinger, ringFinger, pinky: ",
        ]);
    }
}
