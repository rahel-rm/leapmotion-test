import React from "react";
import prettify from "../../util/prettify.js";

export default class FingerDataExplanation extends React.Component {
    render() {
        return (
            <div>
                <h3>Finger keys</h3>
                <pre>{this.explainData()}</pre>
            </div>
        );
    }

    explainData() {
        return prettify([
            "valid: ",
            "id: ",
            "handId: ",
            "length: Length of finger in mm",
            "tool: // DEPRECATED",
            "width: Average width of finger, ",
            "direction: The direction is expressed as a unit vector pointing in the same direction as the tip.",
            "stabilizedTipPosition: Stabilized based on the velocity of the pointable to make precise positioning easier.",
            "tipPosition: The tip position in millimeters from the Leap origin.",
            "tipVelocity: The rate of change of the tip position in millimeters/second.",
            "touchZone: The touch zone is based on a floating touch plane that adapts to the user’s finger movement and hand posture",
            "touchDistance: A value proportional to the distance between this Pointable object and the adaptive touch plane.",
            "timeVisible: The amount of time this pointable has been continuously visible to the Leap Motion controller in seconds.",
            "dipPosition: The physical position of the distal interphalangeal joint of the finger. " +
                "This point is the base of the distal bone (closest to the intermediate phalanx).",
            "pipPosition: The physical position of the proximal interphalangeal joint of the finger. " +
                "This position is the joint between the proximal and the intermediate phalanges.",
            "mcpPosition: The physical position of the metacarpophalangeal joint, or knuckle, of the finger. " +
                "This position is the joint between the metacarpal and proximal phalanx bones.",
            "carpPosition: The physical position of the base end of the metacarpal bone of the finger (closest to the wrist).",
            "extended: True, if the finger is a pointing, or extended, posture.",
            "type: The anatomical name of this finger:  [\"thumb\", \"index\", \"middle\", \"ring\", \"pinky\"]",
            "finger: ",
            "positions: ",
            "metacarpal: The metacarpal bone of the finger (the bone within the palm of the hand).",
            "proximal: The proximal phalanx of the finger (the phalanx bone closest to the body).s",
            "medial: The intermediate phalanx of the finger (the bone in the middle of the finger).",
            "distal: The distal phalanx of the finger (the bone most distant from the body).",
            "bones: The finger bones ordered from wrist to finger tip (metacarpal, proximal phalanx, intermediate phalanx, and distal ph).",
        ]);
    }
}
