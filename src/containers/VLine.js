import React from "react";

export default class Leap extends React.Component {
    render() {
        return (
            <div style={this.style()} {...this.props}>
                {this.props.children}
            </div>
        );
    }

    style() {
        return {
            borderRight: "1px solid #aaa",
            display: "inline-block",
            padding: "0 1em",
            verticalAlign: "top",
        };
    }
}
