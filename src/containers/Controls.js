import React from "react";
import PropTypes from "prop-types";

export default class Controls extends React.Component {
    static propTypes = {
        controller: PropTypes.object,
        connected: PropTypes.bool,
    };

    constructor(props) {
        super(props);
        this.state = {
            frameRate: 0,
        };
    }

    render() {
        return (
            <p>
                {this.props.connected ? (<span>Framerate: {this.state.frameRate}</span>) : "Not streaming."}
                {this.renderControls()}
            </p>
        );
    }

    renderControls() {
        if (!this.props.connected) {
            return <button onClick={::this.connect}>Connect</button>;
        }
        else {
            return <button onClick={::this.disconnect}>Disconnect</button>;
        }
    }

    connect() {
        this.initFrameRateUpdater(this.props.controller);
        this.props.controller.connect();
    }

    disconnect() {
        this.props.controller.disconnect();
        this.setState({frameRate: 0});
    }

    initFrameRateUpdater(controller) {
        if (!controller || this.state.intervalAttached) {
            return;
        }

        let interval = null;
        controller.on("streamingStarted", () => {
            interval = setInterval(() => {
                if (!controller || !controller.frame) {
                    return;
                }

                const frame = controller.frame();
                if (frame.data) {
                    this.setState({frameRate: controller.frame().data.currentFrameRate});
                }
            }, 1000);
        });

        this.setState({intervalAttached: true});

        controller.on("streamingStopped", () => {
            clearInterval(interval);
            this.setState({intervalAttached: false});
        });
    }
}
