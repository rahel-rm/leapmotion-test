import React from "react";
import LeapJS from "leapjs";
import _ from "lodash";
import prettify from "../util/prettify";
import leapEventNotifier from "../util/leapEventNotifier";
import {decycle} from "../util/cycle.js";
import PalmDataExplanation from "./docs/PalmDataExplanation.js";
import FingerDataExplanation from "./docs/FingerDataExplanation.js";
import LineChart from "./LineChart.js";
import Collapse from "./Collapse.js";
import Controls from "./Controls.js";
import HandEntryPlugin from "./HandEntryPlugin.js";
import PinchCounter from "./PinchCounter.js";
import DownloadFrame from "./DownloadFrame.js";
import TouchZone from "./TouchZone.js";

export default class Leap extends React.Component {
    constructor(props) {
        super(props);
        this.data = [];
        this.lineData = new Map();
        this.state = {
            index: 0,
            data: null,
            keys: null,
            maxV: "[0, 0, 0]",
        };
    }

    componentWillMount() {
        const controllerOptions = {
            frameEventName: "animationFrame",
            inNode: false,
        };
        const controller = new LeapJS.Controller(controllerOptions);

        controller.on("frame", () => {
            this.parseFrame(controller.frame());
        });

        leapEventNotifier(controller);
        this.controller = controller;
    }

    parseFrame(frame) {
        this.setState({index: this.state.index + 1, frame});
        const hands = decycle(frame.hands);
        if (hands.length) {
            const hand = hands[0];

            if (!this.once && hand.roll) {
                this.once = 1;
            }

            if (hand.pointables.length) {
                const finger = decycle(_.omit(hand.pointables[0], "frame"));
                this.setState({finger});
            }

            if (!this.state.keys) {
                this.setState({keys: prettify(Object.keys(hand))});
            }

            const data = _.pick(hand, ["palmPosition", "direction", "palmVelocity"]);

            this.findMaxV(data);
            this.setLineData(hand, "pinchStrength");
            this.setLineData(hand, "palmNormal");
            this.setState({data: prettify(data)});
        }

    }

    absBigger(a, b) {
        return Math.max(Math.abs(a), Math.abs(b));
    }

    render() {
        return (
            <div>
                <Controls controller={this.controller} connected={this.controller && this.controller.streaming()} />
                <DownloadFrame frame={this.state.frame} />
                <HandEntryPlugin controller={this.controller} />
                <Collapse title="Docs">
                    <PalmDataExplanation />
                    <FingerDataExplanation />
                </Collapse>
                <Collapse title="Data">
                    <h3>Hand values</h3>
                    <pre>{this.state.data}</pre>
                    <h3>Finger values: {this.getFinger()}</h3>
                    <pre>{prettify(this.state.finger)}</pre>
                </Collapse>
                <Collapse title="TouchZone">
                    <TouchZone frame={this.state.frame} />
                </Collapse>
                <Collapse title="PinchCounter">
                    <PinchCounter frame={this.state.frame} />
                </Collapse>
                <Collapse title="Graphs">
                    {Array.from(this.lineData).map(([key, data]) =>
                        (<Collapse key={key} title={key}>
                            <h3>{key}</h3>
                            <pre>{prettify(this.state[this.state.key])}</pre>
                            <LineChart data={data} title={key} />
                        </Collapse>)
                    )}
                </Collapse>
            </div>
        );
    }

    getFinger() {
        if (!this.state.finger) {
            return "missing";
        }
        return ["thumb", "index", "middle", "ring", "pinky"][this.state.finger.type];
    }

    setLineData(hand, key) {
        const data = hand[key];
        const t = this.state.index;
        const n = 15;

        this.setState({key: key, [key]: data});

        const prevData = this.lineData.get(key) || [
            {name: "X", values: [{x: 0, y: 0}]},
            {name: "Y", values: [{x: 0, y: 0}]},
            {name: "Z", values: [{x: 0, y: 0}]},
        ];

        this.lineData.set(key, prevData.map((series, index) => {
            const values = series.values;
            if (!Array.isArray(data)) {
                values.push({x: t, y: data});
            }
            else {
                values.push({x: t, y: data[index]});
            }
            return {...series, values: values.slice(-n)};
        }));
    }

    findMaxV(data) {
        const prevMaxV = this.state.maxV;
        const maxV = data.palmVelocity.map((dir, index) => {
            return this.absBigger(dir, prevMaxV[index]);
        });
        this.setState({maxV});
    }
}
