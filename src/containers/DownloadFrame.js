import _ from "lodash";
import React from "react";
import PropTypes from "prop-types";
import {decycle} from "../util/cycle.js";
import prettify from "../util/prettify.js";

export default class DownloadFrame extends React.Component {
    static propTypes = {
        frame: PropTypes.object,
    };

    render() {
        return (
            <div>Export:
                <button onClick={() => this.handleClick("full")}>Full</button>
                <button onClick={() => this.handleClick("data")}>Data</button>
                <button onClick={() => this.handleClick("dump")}>Dump</button>
                <button onClick={() => this.handleClick("controller")}>Controller</button>
            </div>
        );
    }

    handleClick(scope) {
        const frame = extractData(this.props.frame, scope);
        const fileName = `frame-${scope}-${this.props.frame.id}.txt`;
        download(prettify(decycle(frame)), fileName);
    }
}

function extractData(frame, scope) {
    switch (scope) {
        case "full":
            return frame;
        case "data":
            return _.omit(frame, "controller");
        case "dump":
            return frame.dump();
        case "controller":
            return _.pick(frame, "controller");
    }
}

function download(data, filename) {
    const element = document.createElement("a");
    element.setAttribute("href", "data:text/plain;charset=utf-8," + encodeURIComponent(data));
    element.setAttribute("download", filename);

    element.style.display = "none";
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}
