import React from "react";
import PropTypes from "prop-types";

export default class TouchZone extends React.Component {

    static propTypes = {
        frame: PropTypes.object,
    }

    render() {
        return (
            <div>
                <h4>Zone: {this.getZone()}</h4>
                <h4>Distance: {this.getDistance()}</h4>
            </div>
        );
    }

    getZone() {
        const frame = this.props.frame;
        if (frame && frame.pointables.length > 0) {
            const touchZone = frame.pointables[0].touchZone;
            return touchZone;
        }
    }

    getDistance() {
        const frame = this.props.frame;
        if (frame && frame.pointables.length > 0) {
            const touchDistance = frame.pointables[0].touchDistance;
            return touchDistance;
        }
    }
}
