import React from "react";
import PropTypes from "prop-types";
import prettify from "../util/prettify";

const TIMEOUT_MILLIS = 200;
const TIMER_TEST_TOTAL = 1000 * 15; // 15 seconds
export default class PinchCounter extends React.Component {
    static propTypes = {
        frame: PropTypes.object,
    }

    constructor(props) {
        super(props);
        this.state = {
            lastUpdate: 0,
            pinches: 0,
            isOpening: true,
            timing: {
                start: undefined,
                pinchesSinceStart: 0,
            },
            recordedTimes: [],
        };
    }

    componentWillReceiveProps(nextProps) {
        if (this.isPinchDetected(this.props.frame, nextProps.frame)) {
            this.setState({pinches: this.state.pinches + 1});
            this.runPinchTimer();
        }
    }

    shouldComponentUpdate() {
        if (this.state.lastUpdate < new Date().getTime() - TIMEOUT_MILLIS) {
            this.setState({lastUpdate: new Date().getTime()});
            return true;
        }
        return false;
    }

    render() {
        return (
            <div>
                <p>Total Pinches: {this.state.pinches}</p>
                <p>Test Pinches: {this.state.timing.pinchesSinceStart}</p>
                <p>Test Time: {new Date().getTime() - this.state.timing.start}</p>
                <p>Recorded Times: {prettify(this.state.recordedTimes)}</p>
                <p>Opening: {prettify(this.state.isOpening)}</p>
            </div>
        );
    }

    isPinchDetected(prevFrame, newFrame) {
        const pinchIsOpening = this.isPinchOpening(prevFrame, newFrame);
        // console.log(pinchIsOpening);
        if (pinchIsOpening !== this.state.isOpening) {
            this.setState({isOpening: pinchIsOpening});
            if (!pinchIsOpening) {
                return true;
            }
        }
        return false;
    }

    isPinchOpening(prevFrame, nextFrame) {
        const prevPinch = this.getPinchStrength(prevFrame);
        const nextPinch = this.getPinchStrength(nextFrame);
        if (isNaN(prevPinch) || isNaN(nextPinch)) {
            return false;
        }
        if (prevPinch == nextPinch) {
            return this.state.isOpening;
        }
        return prevPinch > nextPinch;
    }

    getPinchStrength(frame) {
        if (!frame || !frame.hands.length) {
            return undefined;
        }

        return frame.hands[0].pinchStrength;
    }

    runPinchTimer() {
        const timing = this.state.timing;
        if (!timing.start) {
            this.setState({
                timing: {
                    start: new Date().getTime(),
                    pinchesSinceStart: 1,
                },
            });
        }
        else if (new Date().getTime() - timing.start > TIMER_TEST_TOTAL) {
            this.setState({
                timing: {
                    start: undefined,
                    pinchesSinceStart: 0,
                },
                recordedTimes: this.state.recordedTimes.concat(this.state.timing.pinchesSinceStart),
            });
        }
        else {
            this.setState({
                timing: {
                    start: timing.start,
                    pinchesSinceStart: timing.pinchesSinceStart + 1,
                },
            });
        }
    }
}
