/* eslint-disable no-console */
export default function notifier(controller) {
    const eventsToNotify = [
        "blur",
        "connect",
        "deviceAttached",
        // "deviceConnected", // deprecated
        // "deviceDisconnected", // deprecated
        "deviceRemoved",
        "deviceStopped",
        "deviceStreaming",
        "disconnect",
        "focus",
        // "frame",
        // "gesture", // deprecated
        // "frameEnd",
        "protocol",
        "streamingStarted",
        "streamingStopped",
    ];

    eventsToNotify.forEach((event) => {
        controller.on(event, () => console.info("Event: ", event));
    });

    controller.on("streamingStarted", () => console.info("Speed: ", controller.frame().frameEventName));
}

