export default function prettify(object) {
    return JSON.stringify(object, null, 2);
}
