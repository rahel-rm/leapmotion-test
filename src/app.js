import React from "react";
import ReactDom from "react-dom";
import { AppContainer } from "react-hot-loader";

import Leap from "./containers/leap";

window.onload = function() {
    ReactDom.render(
        <AppContainer>
            <Leap />
        </AppContainer>,
        document.getElementById("root")
    );

    if (module.hot) {
        module.hot.accept("./containers/leap", () => {
            ReactDom.render(
                <AppContainer>
                    <Leap />
                </AppContainer>,
                document.getElementById("root")
            );
        });
    }
};
