{
  "valid": true,
  "id": 676922,
  "timestamp": 6022703581,

  /**
    https://developer.leapmotion.com/documentation/javascript/api/Leap.Hand.html
  */
  "hands": [
    {
      /**
       A unique ID assigned to this Hand object, whose value remains the same
       across consecutive frames while the tracked hand remains visible. If
       tracking is lost (for example, when a hand is occluded by another hand or
       when it is withdrawn from or reaches the edge of the Leap field of view),
       the Leap may assign a new ID when it detects the hand in a future frame.
       */
      "id": 87,

      /**
       * The center position of the palm in millimeters from the Leap origin.
       */
      "palmPosition": [
        -94.1673,
        235.386,
        -35.5208
      ],

      /*
        Type:	number[] – a 3-element array representing a unit direction vector.

        The direction from the palm position toward the fingers.
        The direction is expressed as a unit vector pointing in the same direction
        as the directed line from the palm position to the fingers.
      */
      "direction": [
        -0.263457,
        0.274759,
        -0.924715
      ],

      /**
       * The rate of change of the palm position in millimeters/second.
       */
      "palmVelocity": [
        -121.515,
        352.596,
        -125.006
      ],

      /**
        The normal vector to the palm. If your hand is flat, this vector will
        point downward, or “out” of the front surface of your palm.
        The direction is expressed as a unit vector pointing in the same
        direction as the palm normal (that is, a vector orthogonal to the palm).
       */
      "palmNormal": [
        -0.122934,
        -0.960328,
        -0.250315
      ],
      "sphereCenter": [
        -92.4809,
        283.862,
        -70.8808
      ],
      "sphereRadius": 70.1422,
      "valid": true,
      "pointables": [
        {
          "valid": true,
          "id": 870,
          "handId": 87,
          "length": 46.5396,
          "tool": false,
          "width": 16.9368,
          "direction": [
            0.547794,
            -0.0212038,
            -0.836344
          ],
          "stabilizedTipPosition": [
            -5.01442,
            214.959,
            -53.7534
          ],
          "tipPosition": [
            -15.9068,
            210.586,
            -71.8616
          ],
          "tipVelocity": [
            -237.095,
            230.434,
            -315.91
          ],
          "touchZone": "hovering",
          "touchDistance": -0.17211,
          "timeVisible": 7.37512,
          "dipPosition": [
            -28.5599,
            210.673,
            -61.0953
          ],
          "pipPosition": [
            -45.2387,
            211.319,
            -35.6309
          ],
          "mcpPosition": [
            -63.2254,
            214.649,
            5.49555
          ],
          "carpPosition": [
            -63.2254,
            214.649,
            5.49555
          ],
          "extended": true,
          "type": 0,
          "finger": true,
          "positions": [
            null,
            null,
            null,
            null,
            null
          ],
          "metacarpal": {
            "_center": null,
            "_matrix": null,
            "type": 0,
            "width": 16.9368,
            "length": 0,
            "basis": [
              [
                -0.326469,
                0.931765,
                0.158847
              ],
              [
                0.927828,
                0.283829,
                0.242027
              ],
              [
                -0.180427,
                -0.226397,
                0.957178
              ]
            ]
          },
          "proximal": {
            "_center": null,
            "_matrix": null,
            "type": 1,
            "width": 16.9368,
            "length": 45.01105604729243,
            "basis": [
              [
                -0.243447,
                0.952379,
                -0.1836
              ],
              [
                0.88377,
                0.295804,
                0.362562
              ],
              [
                -0.399605,
                0.0739957,
                0.913696
              ]
            ]
          },
          "medial": {
            "_center": null,
            "_matrix": null,
            "type": 2,
            "width": 16.9368,
            "length": 30.447255258889925,
            "basis": [
              [
                -0.243447,
                0.952379,
                -0.1836
              ],
              [
                0.800409,
                0.30418,
                0.516546
              ],
              [
                -0.547794,
                0.0212038,
                0.836344
              ]
            ]
          },
          "distal": {
            "_center": null,
            "_matrix": null,
            "type": 3,
            "nextJoint": [
              -13.5645,
              211.707,
              -75.6159
            ],
            "width": 16.9368,
            "length": 20.899258396412055,
            "basis": [
              [
                -0.243447,
                0.952379,
                -0.1836
              ],
              [
                0.652624,
                0.300879,
                0.69538
              ],
              [
                -0.717507,
                -0.0494663,
                0.694793
              ]
            ]
          },
          "bones": [
            null,
            null,
            null,
            null
          ]
        },
        {
          "valid": true,
          "id": 871,
          "handId": 87,
          "length": 52.5148,
          "tool": false,
          "width": 16.178,
          "direction": [
            -0.0968846,
            -0.143607,
            -0.984881
          ],
          "stabilizedTipPosition": [
            -82.7704,
            241.04,
            -131.209
          ],
          "tipPosition": [
            -83.3464,
            246.153,
            -131.523
          ],
          "tipVelocity": [
            -303.361,
            454.803,
            -122.429
          ],
          "touchZone": "hovering",
          "touchDistance": 0.309699,
          "timeVisible": 7.37512,
          "dipPosition": [
            -82.6946,
            247.383,
            -121.037
          ],
          "pipPosition": [
            -80.6035,
            250.483,
            -99.7794
          ],
          "mcpPosition": [
            -78.2674,
            242.373,
            -62.3541
          ],
          "carpPosition": [
            -70.5648,
            234.586,
            3.0171
          ],
          "extended": true,
          "type": 1,
          "finger": true,
          "positions": [
            null,
            null,
            null,
            null,
            null
          ],
          "metacarpal": {
            "_center": null,
            "_matrix": null,
            "type": 0,
            "width": 16.178,
            "length": 66.28243511821212,
            "basis": [
              [
                -0.987458,
                0.0931869,
                0.12745
              ],
              [
                0.106879,
                0.988693,
                0.105178
              ],
              [
                0.116208,
                -0.11748,
                0.986253
              ]
            ]
          },
          "proximal": {
            "_center": null,
            "_matrix": null,
            "type": 1,
            "width": 16.178,
            "length": 38.365121442529016,
            "basis": [
              [
                -0.991667,
                0.098348,
                0.0832119
              ],
              [
                0.113529,
                0.972442,
                0.203638
              ],
              [
                0.0608914,
                -0.211388,
                0.975504
              ]
            ]
          },
          "medial": {
            "_center": null,
            "_matrix": null,
            "type": 2,
            "width": 16.178,
            "length": 21.583981490216313,
            "basis": [
              [
                -0.991667,
                0.098348,
                0.0832119
              ],
              [
                0.0849112,
                0.984736,
                -0.151939
              ],
              [
                0.0968846,
                0.143607,
                0.984881
              ]
            ]
          },
          "distal": {
            "_center": null,
            "_matrix": null,
            "type": 3,
            "nextJoint": [
              -84.4598,
              241.346,
              -134.937
            ],
            "width": 16.178,
            "length": 15.256844367037381,
            "basis": [
              [
                -0.991667,
                0.098348,
                0.0832119
              ],
              [
                0.0566728,
                0.913094,
                -0.403792
              ],
              [
                0.115692,
                0.395711,
                0.911059
              ]
            ]
          },
          "bones": [
            null,
            null,
            null,
            null
          ]
        },
        {
          "valid": true,
          "id": 872,
          "handId": 87,
          "length": 59.8364,
          "tool": false,
          "width": 15.889,
          "direction": [
            -0.260529,
            0.0877694,
            -0.961468
          ],
          "stabilizedTipPosition": [
            -117.85,
            260.119,
            -131.527
          ],
          "tipPosition": [
            -119.579,
            264.492,
            -130.058
          ],
          "tipVelocity": [
            -261.736,
            544.233,
            -31.8748
          ],
          "touchZone": "hovering",
          "touchDistance": 0.329333,
          "timeVisible": 7.37512,
          "dipPosition": [
            -115.273,
            262.853,
            -119.159
          ],
          "pipPosition": [
            -108.658,
            260.624,
            -94.7443
          ],
          "mcpPosition": [
            -97.0556,
            245.219,
            -56.2638
          ],
          "carpPosition": [
            -81.3151,
            237.19,
            4.05879
          ],
          "extended": true,
          "type": 2,
          "finger": true,
          "positions": [
            null,
            null,
            null,
            null,
            null
          ],
          "metacarpal": {
            "_center": null,
            "_matrix": null,
            "type": 0,
            "width": 15.889,
            "length": 62.857322927071124,
            "basis": [
              [
                -0.966334,
                -0.0934646,
                0.239714
              ],
              [
                -0.0590761,
                0.987395,
                0.146838
              ],
              [
                0.250417,
                -0.127733,
                0.959675
              ]
            ]
          },
          "proximal": {
            "_center": null,
            "_matrix": null,
            "type": 1,
            "width": 15.889,
            "length": 43.04275306030041,
            "basis": [
              [
                -0.962974,
                -0.0951282,
                0.252253
              ],
              [
                0.00523794,
                0.928899,
                0.370296
              ],
              [
                0.269543,
                -0.357907,
                0.894007
              ]
            ]
          },
          "medial": {
            "_center": null,
            "_matrix": null,
            "type": 2,
            "width": 15.889,
            "length": 25.392995925845387,
            "basis": [
              [
                -0.962974,
                -0.0951282,
                0.252253
              ],
              [
                -0.0693226,
                0.991588,
                0.109303
              ],
              [
                0.260529,
                -0.0877694,
                0.961468
              ]
            ]
          },
          "distal": {
            "_center": null,
            "_matrix": null,
            "type": 3,
            "nextJoint": [
              -119.285,
              260.653,
              -135.305
            ],
            "width": 15.889,
            "length": 16.781819329262248,
            "basis": [
              [
                -0.962974,
                -0.0951282,
                0.252253
              ],
              [
                -0.124587,
                0.986799,
                -0.103473
              ],
              [
                0.23908,
                0.131069,
                0.962113
              ]
            ]
          },
          "bones": [
            null,
            null,
            null,
            null
          ]
        },
        {
          "valid": true,
          "id": 873,
          "handId": 87,
          "length": 57.5343,
          "tool": false,
          "width": 15.1193,
          "direction": [
            -0.48908,
            0.180671,
            -0.853322
          ],
          "stabilizedTipPosition": [
            -151.758,
            262.794,
            -108.758
          ],
          "tipPosition": [
            -153.157,
            262.487,
            -108.019
          ],
          "tipVelocity": [
            -191.057,
            669.94,
            49.3921
          ],
          "touchZone": "hovering",
          "touchDistance": 0.386743,
          "timeVisible": 7.37512,
          "dipPosition": [
            -146.267,
            264.686,
            -97.2486
          ],
          "pipPosition": [
            -134.168,
            260.217,
            -76.1393
          ],
          "mcpPosition": [
            -114.379,
            244.561,
            -45.2334
          ],
          "carpPosition": [
            -92.2059,
            237.055,
            6.11808
          ],
          "extended": true,
          "type": 3,
          "finger": true,
          "positions": [
            null,
            null,
            null,
            null,
            null
          ],
          "metacarpal": {
            "_center": null,
            "_matrix": null,
            "type": 0,
            "width": 15.1193,
            "length": 56.43545780624447,
            "basis": [
              [
                -0.911729,
                -0.185409,
                0.36657
              ],
              [
                -0.119953,
                0.973619,
                0.194107
              ],
              [
                0.392888,
                -0.133002,
                0.909917
              ]
            ]
          },
          "proximal": {
            "_center": null,
            "_matrix": null,
            "type": 1,
            "width": 15.1193,
            "length": 39.89849009436321,
            "basis": [
              [
                -0.867945,
                -0.197762,
                0.45559
              ],
              [
                0.025575,
                0.898292,
                0.438654
              ],
              [
                0.496002,
                -0.392379,
                0.77461
              ]
            ]
          },
          "medial": {
            "_center": null,
            "_matrix": null,
            "type": 2,
            "width": 15.1193,
            "length": 24.737831523599624,
            "basis": [
              [
                -0.867945,
                -0.197762,
                0.45559
              ],
              [
                -0.086443,
                0.963456,
                0.253534
              ],
              [
                0.48908,
                -0.180671,
                0.853322
              ]
            ]
          },
          "distal": {
            "_center": null,
            "_matrix": null,
            "type": 3,
            "nextJoint": [
              -153.843,
              263.756,
              -112.085
            ],
            "width": 15.1193,
            "length": 16.68470679874237,
            "basis": [
              [
                -0.867945,
                -0.197762,
                0.45559
              ],
              [
                -0.201263,
                0.978662,
                0.0413916
              ],
              [
                0.454054,
                0.0557675,
                0.889227
              ]
            ]
          },
          "bones": [
            null,
            null,
            null,
            null
          ]
        },
        {
          "valid": true,
          "id": 874,
          "handId": 87,
          "length": 45.1058,
          "tool": false,
          "width": 13.4302,
          "direction": [
            -0.754481,
            0.0830332,
            -0.651049
          ],
          "stabilizedTipPosition": [
            -175.167,
            246.534,
            -72.3983
          ],
          "tipPosition": [
            -174.65,
            243.942,
            -73.1733
          ],
          "tipVelocity": [
            -160.878,
            687.645,
            55.2925
          ],
          "touchZone": "hovering",
          "touchDistance": 0.333729,
          "timeVisible": 7.37512,
          "dipPosition": [
            -167.371,
            249.287,
            -63.7951
          ],
          "pipPosition": [
            -154.193,
            247.836,
            -52.4239
          ],
          "mcpPosition": [
            -129.59,
            239.632,
            -34.4127
          ],
          "carpPosition": [
            -102.735,
            230.516,
            9.46113
          ],
          "extended": true,
          "type": 4,
          "finger": true,
          "positions": [
            null,
            null,
            null,
            null,
            null
          ],
          "metacarpal": {
            "_center": null,
            "_matrix": null,
            "type": 0,
            "width": 13.4302,
            "length": 52.241797823858434,
            "basis": [
              [
                -0.827692,
                -0.357861,
                0.432274
              ],
              [
                -0.225114,
                0.917328,
                0.32838
              ],
              [
                0.514051,
                -0.174487,
                0.839825
              ]
            ]
          },
          "proximal": {
            "_center": null,
            "_matrix": null,
            "type": 1,
            "width": 13.4302,
            "length": 31.575568885453205,
            "basis": [
              [
                -0.624816,
                -0.394522,
                0.673764
              ],
              [
                -0.0499707,
                0.881383,
                0.469752
              ],
              [
                0.779172,
                -0.25984,
                0.570416
              ]
            ]
          },
          "medial": {
            "_center": null,
            "_matrix": null,
            "type": 2,
            "width": 13.4302,
            "length": 17.46623240541588,
            "basis": [
              [
                -0.624816,
                -0.394522,
                0.673764
              ],
              [
                -0.200908,
                0.915127,
                0.349539
              ],
              [
                0.754481,
                -0.0830332,
                0.651049
              ]
            ]
          },
          "distal": {
            "_center": null,
            "_matrix": null,
            "type": 3,
            "nextJoint": [
              -177.763,
              246.817,
              -74.8781
            ],
            "width": 13.4302,
            "length": 15.392447920977352,
            "basis": [
              [
                -0.624816,
                -0.394522,
                0.673764
              ],
              [
                -0.392164,
                0.904772,
                0.166114
              ],
              [
                0.675139,
                0.160435,
                0.720033
              ]
            ]
          },
          "bones": [
            null,
            null,
            null,
            null
          ]
        }
      ],
      "fingers": [
        null,
        null,
        null,
        null,
        null
      ],
      "arm": {
        "_center": null,
        "_matrix": null,
        "type": 4,
        "prevJoint": [
          -170.07,
          126.316,
          240.268
        ],
        "nextJoint": [
          -88.1824,
          219.356,
          30.7175
        ],
        "width": 54.5643,
        "length": 243.46135768127556,
        "basis": [ // käsivarre asukoht
          [ // X, küljesuunas. Paremal käel paremale suunas, vasakul vasakule
            -0.924283,
            0.309113,
            -0.223942
          ],
          [ // Y, üles suunas
            0.180477,
            0.870866,
            0.457188
          ],
          [ // Z, mööda kätt, õla suunas
            -0.336347,
            -0.382155,
            0.860714
          ]
        ]
      },
      "tools": [],
      "_translation": [
        -126.632,
        -28.922,
        -190.267
      ],
      "_rotation": [
        0.412001,
        -0.643727,
        -0.644881,
        0.179843,
        0.751266,
        -0.635024,
        0.893259,
        0.145653,
        0.425292
      ],
      "_scaleFactor": 2.12864,
      "timeVisible": 7.37512,
      "stabilizedPalmPosition": [
        -92.8867,
        232.211,
        -34.1213
      ],
      "type": "left",

      /**
        The strength of a grab hand pose.

        The strength is zero for an open hand, and blends to 1.0 when a grabbing
        hand pose is recognized. The following example uses grabStrength to
        determine whether a hand is open or closed. The example also compares
        the current value to the average value from recent history to determine
        whether the hand is opening or closing (if the hand isn’t fully open or closed).
      */
      "grabStrength": 0,
      "pinchStrength": 0,

      /**
          How well the internal hand model fits the observed data.

          A low value indicates that there are significant discrepancies;
          finger positions, even hand identification could be incorrect.
          The significance of the confidence value to your application can vary with context.
      */
      "confidence": 1
    }
  ],
  "handsMap": {},
  "pointables": [
    null,
    null,
    null,
    null,
    null
  ],
  "tools": [],
  "fingers": [
    null,
    null,
    null,
    null,
    null
  ],
  "interactionBox": {
    "valid": true,
    "center": [
      0,
      200,
      0
    ],
    "size": [
      235.247,
      235.247,
      147.751
    ],
    "width": 235.247,
    "height": 235.247,
    "depth": 147.751
  },
  "gestures": [],
  "pointablesMap": {},
  "_translation": [
    -126.632,
    -28.922,
    -190.267
  ],
  "_rotation": [
    0.412001,
    -0.643727,
    -0.644881,
    0.179843,
    0.751266,
    -0.635024,
    0.893259,
    0.145653,
    0.425292
  ],
  "_scaleFactor": 2.12864,
  "data": {
    "currentFrameRate": 110.617,
    "devices": [],
    "gestures": [],
    "hands": [
      {
        "armWidth": 54.5643,
        "confidence": 1,
        "grabAngle": 0.297366,
        "grabStrength": 0,
        "id": 87,

        /**
         * The average outer width of the hand (not including fingers or thumb) in millimeters.
         */
        "palmWidth": 81.9772,
        "pinchDistance": 76.0909,

        /**
         * The holding strength of a pinch hand pose.
          The strength is zero for an open hand, and blends to 1.0 when a pinching
          hand pose is recognized. Pinching can be done between the thumb and
          any other finger of the same hand.
          The following example compares the distances between the tip of the
          thumb and other fingers to determine which finger is pinching.
         */
        "pinchStrength": 0,
        "r": [
          [
            0.412001,
            -0.643727,
            -0.644881
          ],
          [
            0.179843,
            0.751266,
            -0.635024
          ],
          [
            0.893259,
            0.145653,
            0.425292
          ]
        ],
        "s": 2.12864,
        "sphereRadius": 70.1422,
        "timeVisible": 7.37512,
        "type": "left"
      }
    ],
    "id": 676922,
    "interactionBox": {},

    /**
     * The list of Pointable objects (fingers) detected in this frame that are
     associated with this hand, given in arbitrary order. The list can be empty
     if no fingers associated with this hand are detected.

     The following example identifies each pointable by finger name.
      ["thumb", "index", "middle", "ring", "pinky"]
     */
    "pointables": [
      {
        "bases": [
          null,
          null,
          null,
          null
        ],
        "extended": true,
        "handId": 87,
        "id": 870,
        "length": 46.5396,
        "timeVisible": 7.37512,
        "tool": false,
        "touchDistance": -0.17211,
        "touchZone": "hovering",
        "type": 0,
        "width": 16.9368
      },
      {
        "bases": [
          null,
          null,
          null,
          null
        ],
        "extended": true,
        "handId": 87,
        "id": 871,
        "length": 52.5148,
        "timeVisible": 7.37512,
        "tool": false,
        "touchDistance": 0.309699,
        "touchZone": "hovering",
        "type": 1,
        "width": 16.178
      },
      {
        "bases": [
          null,
          null,
          null,
          null
        ],
        "extended": true,
        "handId": 87,
        "id": 872,
        "length": 59.8364,
        "timeVisible": 7.37512,
        "tool": false,
        "touchDistance": 0.329333,
        "touchZone": "hovering",
        "type": 2,
        "width": 15.889
      },
      {
        "bases": [
          null,
          null,
          null,
          null
        ],
        "extended": true,
        "handId": 87,
        "id": 873,
        "length": 57.5343,
        "timeVisible": 7.37512,
        "tool": false,
        "touchDistance": 0.386743,
        "touchZone": "hovering",
        "type": 3,
        "width": 15.1193
      },
      {
        "bases": [
          null,
          null,
          null,
          null
        ],
        "extended": true,
        "handId": 87,
        "id": 874,
        "length": 45.1058,
        "timeVisible": 7.37512,
        "tool": false,
        "touchDistance": 0.333729,
        "touchZone": "hovering",
        "type": 4,
        "width": 13.4302
      }
    ],
    "r": [
      [
        0.412001,
        -0.643727,
        -0.644881
      ],
      [
        0.179843,
        0.751266,
        -0.635024
      ],
      [
        0.893259,
        0.145653,
        0.425292
      ]
    ],
    "s": 2.12864,
    "timestamp": 6022703581
  },
  "type": "frame",
  "currentFrameRate": 110.617,
  "controller": {
    "animationFrameRequested": true,
    "suppressAnimationLoop": false,
    "loopWhileDisconnected": true,
    "frameEventName": "animationFrame",
    "useAllPlugins": false,
    "history": {
      "pos": 55608,
      "_buf": [
        {
          "valid": true,
          "id": 824355,
          "timestamp": 7376811696,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.428,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824355,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376811696
          },
          "type": "frame",
          "currentFrameRate": 110.428,
          "historyIdx": 55600
        },
        {
          "valid": true,
          "id": 824357,
          "timestamp": 7376829807,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.431,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824357,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376829807
          },
          "type": "frame",
          "currentFrameRate": 110.431,
          "historyIdx": 55601
        },
        {
          "valid": true,
          "id": 824359,
          "timestamp": 7376847966,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.406,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824359,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376847966
          },
          "type": "frame",
          "currentFrameRate": 110.406,
          "historyIdx": 55602
        },
        {
          "valid": true,
          "id": 824360,
          "timestamp": 7376856964,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.438,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824360,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376856964
          },
          "type": "frame",
          "currentFrameRate": 110.438,
          "historyIdx": 55603
        },
        {
          "valid": true,
          "id": 824362,
          "timestamp": 7376875061,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.445,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824362,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376875061
          },
          "type": "frame",
          "currentFrameRate": 110.445,
          "historyIdx": 55604
        },
        {
          "valid": true,
          "id": 824364,
          "timestamp": 7376893103,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.48,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824364,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376893103
          },
          "type": "frame",
          "currentFrameRate": 110.48,
          "historyIdx": 55605
        },
        {
          "valid": true,
          "id": 824366,
          "timestamp": 7376911233,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.468,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824366,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376911233
          },
          "type": "frame",
          "currentFrameRate": 110.468,
          "historyIdx": 55606
        },
        {
          "valid": true,
          "id": 824368,
          "timestamp": 7376929251,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.513,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824368,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376929251
          },
          "type": "frame",
          "currentFrameRate": 110.513,
          "historyIdx": 55607
        },
        {
          "valid": true,
          "id": 824001,
          "timestamp": 7373558002,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.55,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824001,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7373558002
          },
          "type": "frame",
          "currentFrameRate": 110.55,
          "historyIdx": 55408
        },
        {
          "valid": true,
          "id": 824003,
          "timestamp": 7373575991,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.605,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824003,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7373575991
          },
          "type": "frame",
          "currentFrameRate": 110.605,
          "historyIdx": 55409
        },
        {
          "valid": true,
          "id": 824005,
          "timestamp": 7373594167,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.559,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824005,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7373594167
          },
          "type": "frame",
          "currentFrameRate": 110.559,
          "historyIdx": 55410
        },
        {
          "valid": true,
          "id": 824006,
          "timestamp": 7373603129,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.604,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824006,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7373603129
          },
          "type": "frame",
          "currentFrameRate": 110.604,
          "historyIdx": 55411
        },
        {
          "valid": true,
          "id": 824008,
          "timestamp": 7373621253,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.586,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824008,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7373621253
          },
          "type": "frame",
          "currentFrameRate": 110.586,
          "historyIdx": 55412
        },
        {
          "valid": true,
          "id": 824010,
          "timestamp": 7373639440,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.53,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824010,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7373639440
          },
          "type": "frame",
          "currentFrameRate": 110.53,
          "historyIdx": 55413
        },
        {
          "valid": true,
          "id": 824012,
          "timestamp": 7373657394,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.603,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824012,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7373657394
          },
          "type": "frame",
          "currentFrameRate": 110.603,
          "historyIdx": 55414
        },
        {
          "valid": true,
          "id": 824014,
          "timestamp": 7373675552,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.566,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824014,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7373675552
          },
          "type": "frame",
          "currentFrameRate": 110.566,
          "historyIdx": 55415
        },
        {
          "valid": true,
          "id": 824016,
          "timestamp": 7373693607,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.582,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824016,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7373693607
          },
          "type": "frame",
          "currentFrameRate": 110.582,
          "historyIdx": 55416
        },
        {
          "valid": true,
          "id": 824017,
          "timestamp": 7373702696,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.558,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824017,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7373702696
          },
          "type": "frame",
          "currentFrameRate": 110.558,
          "historyIdx": 55417
        },
        {
          "valid": true,
          "id": 824019,
          "timestamp": 7373720673,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.617,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824019,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7373720673
          },
          "type": "frame",
          "currentFrameRate": 110.617,
          "historyIdx": 55418
        },
        {
          "valid": true,
          "id": 824021,
          "timestamp": 7373738795,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.595,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824021,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7373738795
          },
          "type": "frame",
          "currentFrameRate": 110.595,
          "historyIdx": 55419
        },
        {
          "valid": true,
          "id": 824023,
          "timestamp": 7373756881,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.594,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824023,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7373756881
          },
          "type": "frame",
          "currentFrameRate": 110.594,
          "historyIdx": 55420
        },
        {
          "valid": true,
          "id": 824025,
          "timestamp": 7373774989,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.58,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824025,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7373774989
          },
          "type": "frame",
          "currentFrameRate": 110.58,
          "historyIdx": 55421
        },
        {
          "valid": true,
          "id": 824027,
          "timestamp": 7373811184,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 101.632,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824027,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7373811184
          },
          "type": "frame",
          "currentFrameRate": 101.632,
          "historyIdx": 55422
        },
        {
          "valid": true,
          "id": 824028,
          "timestamp": 7373820203,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 102.039,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824028,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7373820203
          },
          "type": "frame",
          "currentFrameRate": 102.039,
          "historyIdx": 55423
        },
        {
          "valid": true,
          "id": 824030,
          "timestamp": 7373838166,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 102.841,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824030,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7373838166
          },
          "type": "frame",
          "currentFrameRate": 102.841,
          "historyIdx": 55424
        },
        {
          "valid": true,
          "id": 824032,
          "timestamp": 7373856295,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 103.49,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824032,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7373856295
          },
          "type": "frame",
          "currentFrameRate": 103.49,
          "historyIdx": 55425
        },
        {
          "valid": true,
          "id": 824034,
          "timestamp": 7373874420,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 104.083,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824034,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7373874420
          },
          "type": "frame",
          "currentFrameRate": 104.083,
          "historyIdx": 55426
        },
        {
          "valid": true,
          "id": 824036,
          "timestamp": 7373892408,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 104.693,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824036,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7373892408
          },
          "type": "frame",
          "currentFrameRate": 104.693,
          "historyIdx": 55427
        },
        {
          "valid": true,
          "id": 824038,
          "timestamp": 7373910559,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 105.17,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824038,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7373910559
          },
          "type": "frame",
          "currentFrameRate": 105.17,
          "historyIdx": 55428
        },
        {
          "valid": true,
          "id": 824040,
          "timestamp": 7373928682,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 105.617,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824040,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7373928682
          },
          "type": "frame",
          "currentFrameRate": 105.617,
          "historyIdx": 55429
        },
        {
          "valid": true,
          "id": 824041,
          "timestamp": 7373937691,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 105.854,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824041,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7373937691
          },
          "type": "frame",
          "currentFrameRate": 105.854,
          "historyIdx": 55430
        },
        {
          "valid": true,
          "id": 824043,
          "timestamp": 7373955661,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 106.321,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824043,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7373955661
          },
          "type": "frame",
          "currentFrameRate": 106.321,
          "historyIdx": 55431
        },
        {
          "valid": true,
          "id": 824045,
          "timestamp": 7373973740,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 106.697,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824045,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7373973740
          },
          "type": "frame",
          "currentFrameRate": 106.697,
          "historyIdx": 55432
        },
        {
          "valid": true,
          "id": 824047,
          "timestamp": 7373991874,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 107.008,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824047,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7373991874
          },
          "type": "frame",
          "currentFrameRate": 107.008,
          "historyIdx": 55433
        },
        {
          "valid": true,
          "id": 824049,
          "timestamp": 7374009899,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 107.349,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824049,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374009899
          },
          "type": "frame",
          "currentFrameRate": 107.349,
          "historyIdx": 55434
        },
        {
          "valid": true,
          "id": 824051,
          "timestamp": 7374028046,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 107.597,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824051,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374028046
          },
          "type": "frame",
          "currentFrameRate": 107.597,
          "historyIdx": 55435
        },
        {
          "valid": true,
          "id": 824052,
          "timestamp": 7374037111,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 107.718,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824052,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374037111
          },
          "type": "frame",
          "currentFrameRate": 107.718,
          "historyIdx": 55436
        },
        {
          "valid": true,
          "id": 824054,
          "timestamp": 7374055091,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 108.018,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824054,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374055091
          },
          "type": "frame",
          "currentFrameRate": 108.018,
          "historyIdx": 55437
        },
        {
          "valid": true,
          "id": 824056,
          "timestamp": 7374073075,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 108.293,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824056,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374073075
          },
          "type": "frame",
          "currentFrameRate": 108.293,
          "historyIdx": 55438
        },
        {
          "valid": true,
          "id": 824058,
          "timestamp": 7374091253,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 108.446,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824058,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374091253
          },
          "type": "frame",
          "currentFrameRate": 108.446,
          "historyIdx": 55439
        },
        {
          "valid": true,
          "id": 824060,
          "timestamp": 7374109361,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 108.619,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824060,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374109361
          },
          "type": "frame",
          "currentFrameRate": 108.619,
          "historyIdx": 55440
        },
        {
          "valid": true,
          "id": 824062,
          "timestamp": 7374127395,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 108.817,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824062,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374127395
          },
          "type": "frame",
          "currentFrameRate": 108.817,
          "historyIdx": 55441
        },
        {
          "valid": true,
          "id": 824063,
          "timestamp": 7374136554,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 108.833,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824063,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374136554
          },
          "type": "frame",
          "currentFrameRate": 108.833,
          "historyIdx": 55442
        },
        {
          "valid": true,
          "id": 824065,
          "timestamp": 7374154524,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 109.045,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824065,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374154524
          },
          "type": "frame",
          "currentFrameRate": 109.045,
          "historyIdx": 55443
        },
        {
          "valid": true,
          "id": 824067,
          "timestamp": 7374172687,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 109.135,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824067,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374172687
          },
          "type": "frame",
          "currentFrameRate": 109.135,
          "historyIdx": 55444
        },
        {
          "valid": true,
          "id": 824069,
          "timestamp": 7374190680,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 109.308,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824069,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374190680
          },
          "type": "frame",
          "currentFrameRate": 109.308,
          "historyIdx": 55445
        },
        {
          "valid": true,
          "id": 824071,
          "timestamp": 7374208784,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 109.41,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824071,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374208784
          },
          "type": "frame",
          "currentFrameRate": 109.41,
          "historyIdx": 55446
        },
        {
          "valid": true,
          "id": 824073,
          "timestamp": 7374226895,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 109.497,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824073,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374226895
          },
          "type": "frame",
          "currentFrameRate": 109.497,
          "historyIdx": 55447
        },
        {
          "valid": true,
          "id": 824075,
          "timestamp": 7374244889,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 109.639,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824075,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374244889
          },
          "type": "frame",
          "currentFrameRate": 109.639,
          "historyIdx": 55448
        },
        {
          "valid": true,
          "id": 824076,
          "timestamp": 7374254055,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 109.615,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824076,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374254055
          },
          "type": "frame",
          "currentFrameRate": 109.615,
          "historyIdx": 55449
        },
        {
          "valid": true,
          "id": 824078,
          "timestamp": 7374271999,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 109.773,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824078,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374271999
          },
          "type": "frame",
          "currentFrameRate": 109.773,
          "historyIdx": 55450
        },
        {
          "valid": true,
          "id": 824080,
          "timestamp": 7374290126,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 109.824,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824080,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374290126
          },
          "type": "frame",
          "currentFrameRate": 109.824,
          "historyIdx": 55451
        },
        {
          "valid": true,
          "id": 824082,
          "timestamp": 7374308136,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 109.93,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824082,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374308136
          },
          "type": "frame",
          "currentFrameRate": 109.93,
          "historyIdx": 55452
        },
        {
          "valid": true,
          "id": 824084,
          "timestamp": 7374326294,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 109.95,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824084,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374326294
          },
          "type": "frame",
          "currentFrameRate": 109.95,
          "historyIdx": 55453
        },
        {
          "valid": true,
          "id": 824086,
          "timestamp": 7374344414,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 109.985,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824086,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374344414
          },
          "type": "frame",
          "currentFrameRate": 109.985,
          "historyIdx": 55454
        },
        {
          "valid": true,
          "id": 824088,
          "timestamp": 7374362386,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.097,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824088,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374362386
          },
          "type": "frame",
          "currentFrameRate": 110.097,
          "historyIdx": 55455
        },
        {
          "valid": true,
          "id": 824089,
          "timestamp": 7374371555,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.05,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824089,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374371555
          },
          "type": "frame",
          "currentFrameRate": 110.05,
          "historyIdx": 55456
        },
        {
          "valid": true,
          "id": 824091,
          "timestamp": 7374389519,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.16,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824091,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374389519
          },
          "type": "frame",
          "currentFrameRate": 110.16,
          "historyIdx": 55457
        },
        {
          "valid": true,
          "id": 824093,
          "timestamp": 7374407673,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.16,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824093,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374407673
          },
          "type": "frame",
          "currentFrameRate": 110.16,
          "historyIdx": 55458
        },
        {
          "valid": true,
          "id": 824095,
          "timestamp": 7374425661,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.248,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824095,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374425661
          },
          "type": "frame",
          "currentFrameRate": 110.248,
          "historyIdx": 55459
        },
        {
          "valid": true,
          "id": 824097,
          "timestamp": 7374443792,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.255,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824097,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374443792
          },
          "type": "frame",
          "currentFrameRate": 110.255,
          "historyIdx": 55460
        },
        {
          "valid": true,
          "id": 824099,
          "timestamp": 7374461872,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.286,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824099,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374461872
          },
          "type": "frame",
          "currentFrameRate": 110.286,
          "historyIdx": 55461
        },
        {
          "valid": true,
          "id": 824100,
          "timestamp": 7374470909,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.303,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824100,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374470909
          },
          "type": "frame",
          "currentFrameRate": 110.303,
          "historyIdx": 55462
        },
        {
          "valid": true,
          "id": 824102,
          "timestamp": 7374489050,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.296,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824102,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374489050
          },
          "type": "frame",
          "currentFrameRate": 110.296,
          "historyIdx": 55463
        },
        {
          "valid": true,
          "id": 824104,
          "timestamp": 7374507032,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.374,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824104,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374507032
          },
          "type": "frame",
          "currentFrameRate": 110.374,
          "historyIdx": 55464
        },
        {
          "valid": true,
          "id": 824106,
          "timestamp": 7374525159,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.368,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824106,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374525159
          },
          "type": "frame",
          "currentFrameRate": 110.368,
          "historyIdx": 55465
        },
        {
          "valid": true,
          "id": 824108,
          "timestamp": 7374543131,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.447,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824108,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374543131
          },
          "type": "frame",
          "currentFrameRate": 110.447,
          "historyIdx": 55466
        },
        {
          "valid": true,
          "id": 824110,
          "timestamp": 7374561262,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.436,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824110,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374561262
          },
          "type": "frame",
          "currentFrameRate": 110.436,
          "historyIdx": 55467
        },
        {
          "valid": true,
          "id": 824111,
          "timestamp": 7374570274,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.46,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824111,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374570274
          },
          "type": "frame",
          "currentFrameRate": 110.46,
          "historyIdx": 55468
        },
        {
          "valid": true,
          "id": 824113,
          "timestamp": 7374588422,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.439,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824113,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374588422
          },
          "type": "frame",
          "currentFrameRate": 110.439,
          "historyIdx": 55469
        },
        {
          "valid": true,
          "id": 824115,
          "timestamp": 7374606482,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.462,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824115,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374606482
          },
          "type": "frame",
          "currentFrameRate": 110.462,
          "historyIdx": 55470
        },
        {
          "valid": true,
          "id": 824117,
          "timestamp": 7374624515,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.502,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824117,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374624515
          },
          "type": "frame",
          "currentFrameRate": 110.502,
          "historyIdx": 55471
        },
        {
          "valid": true,
          "id": 824119,
          "timestamp": 7374642671,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.47,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824119,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374642671
          },
          "type": "frame",
          "currentFrameRate": 110.47,
          "historyIdx": 55472
        },
        {
          "valid": true,
          "id": 824121,
          "timestamp": 7374660657,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.533,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824121,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374660657
          },
          "type": "frame",
          "currentFrameRate": 110.533,
          "historyIdx": 55473
        },
        {
          "valid": true,
          "id": 824123,
          "timestamp": 7374678782,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.519,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824123,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374678782
          },
          "type": "frame",
          "currentFrameRate": 110.519,
          "historyIdx": 55474
        },
        {
          "valid": true,
          "id": 824124,
          "timestamp": 7374687760,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.557,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824124,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374687760
          },
          "type": "frame",
          "currentFrameRate": 110.557,
          "historyIdx": 55475
        },
        {
          "valid": true,
          "id": 824126,
          "timestamp": 7374705904,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.529,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824126,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374705904
          },
          "type": "frame",
          "currentFrameRate": 110.529,
          "historyIdx": 55476
        },
        {
          "valid": true,
          "id": 824128,
          "timestamp": 7374723882,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.59,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824128,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374723882
          },
          "type": "frame",
          "currentFrameRate": 110.59,
          "historyIdx": 55477
        },
        {
          "valid": true,
          "id": 824130,
          "timestamp": 7374742004,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.571,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824130,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374742004
          },
          "type": "frame",
          "currentFrameRate": 110.571,
          "historyIdx": 55478
        },
        {
          "valid": true,
          "id": 824132,
          "timestamp": 7374760167,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.533,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824132,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374760167
          },
          "type": "frame",
          "currentFrameRate": 110.533,
          "historyIdx": 55479
        },
        {
          "valid": true,
          "id": 824134,
          "timestamp": 7374778132,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.602,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824134,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374778132
          },
          "type": "frame",
          "currentFrameRate": 110.602,
          "historyIdx": 55480
        },
        {
          "valid": true,
          "id": 824136,
          "timestamp": 7374796282,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.569,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824136,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374796282
          },
          "type": "frame",
          "currentFrameRate": 110.569,
          "historyIdx": 55481
        },
        {
          "valid": true,
          "id": 824137,
          "timestamp": 7374805270,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.599,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824137,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374805270
          },
          "type": "frame",
          "currentFrameRate": 110.599,
          "historyIdx": 55482
        },
        {
          "valid": true,
          "id": 824139,
          "timestamp": 7374823402,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.572,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824139,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374823402
          },
          "type": "frame",
          "currentFrameRate": 110.572,
          "historyIdx": 55483
        },
        {
          "valid": true,
          "id": 824141,
          "timestamp": 7374841656,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.48,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824141,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374841656
          },
          "type": "frame",
          "currentFrameRate": 110.48,
          "historyIdx": 55484
        },
        {
          "valid": true,
          "id": 824143,
          "timestamp": 7374859512,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.609,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824143,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374859512
          },
          "type": "frame",
          "currentFrameRate": 110.609,
          "historyIdx": 55485
        },
        {
          "valid": true,
          "id": 824145,
          "timestamp": 7374877607,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.601,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824145,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374877607
          },
          "type": "frame",
          "currentFrameRate": 110.601,
          "historyIdx": 55486
        },
        {
          "valid": true,
          "id": 824147,
          "timestamp": 7374895645,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.627,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824147,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374895645
          },
          "type": "frame",
          "currentFrameRate": 110.627,
          "historyIdx": 55487
        },
        {
          "valid": true,
          "id": 824148,
          "timestamp": 7374904727,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.604,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824148,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374904727
          },
          "type": "frame",
          "currentFrameRate": 110.604,
          "historyIdx": 55488
        },
        {
          "valid": true,
          "id": 824150,
          "timestamp": 7374922768,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.626,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824150,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374922768
          },
          "type": "frame",
          "currentFrameRate": 110.626,
          "historyIdx": 55489
        },
        {
          "valid": true,
          "id": 824152,
          "timestamp": 7374958888,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 101.709,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824152,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374958888
          },
          "type": "frame",
          "currentFrameRate": 101.709,
          "historyIdx": 55490
        },
        {
          "valid": true,
          "id": 824154,
          "timestamp": 7374977020,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 102.456,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824154,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374977020
          },
          "type": "frame",
          "currentFrameRate": 102.456,
          "historyIdx": 55491
        },
        {
          "valid": true,
          "id": 824156,
          "timestamp": 7374995167,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 103.127,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824156,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7374995167
          },
          "type": "frame",
          "currentFrameRate": 103.127,
          "historyIdx": 55492
        },
        {
          "valid": true,
          "id": 824157,
          "timestamp": 7375004147,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 103.488,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824157,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375004147
          },
          "type": "frame",
          "currentFrameRate": 103.488,
          "historyIdx": 55493
        },
        {
          "valid": true,
          "id": 824159,
          "timestamp": 7375022233,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 104.101,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824159,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375022233
          },
          "type": "frame",
          "currentFrameRate": 104.101,
          "historyIdx": 55494
        },
        {
          "valid": true,
          "id": 824161,
          "timestamp": 7375040227,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 104.707,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824161,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375040227
          },
          "type": "frame",
          "currentFrameRate": 104.707,
          "historyIdx": 55495
        },
        {
          "valid": true,
          "id": 824163,
          "timestamp": 7375058433,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 105.156,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824163,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375058433
          },
          "type": "frame",
          "currentFrameRate": 105.156,
          "historyIdx": 55496
        },
        {
          "valid": true,
          "id": 824165,
          "timestamp": 7375076382,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 105.694,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824165,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375076382
          },
          "type": "frame",
          "currentFrameRate": 105.694,
          "historyIdx": 55497
        },
        {
          "valid": true,
          "id": 824167,
          "timestamp": 7375094451,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 106.127,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824167,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375094451
          },
          "type": "frame",
          "currentFrameRate": 106.127,
          "historyIdx": 55498
        },
        {
          "valid": true,
          "id": 824168,
          "timestamp": 7375103453,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 106.345,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824168,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375103453
          },
          "type": "frame",
          "currentFrameRate": 106.345,
          "historyIdx": 55499
        },
        {
          "valid": true,
          "id": 824170,
          "timestamp": 7375121620,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 106.671,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824170,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375121620
          },
          "type": "frame",
          "currentFrameRate": 106.671,
          "historyIdx": 55500
        },
        {
          "valid": true,
          "id": 824172,
          "timestamp": 7375139776,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 106.971,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824172,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375139776
          },
          "type": "frame",
          "currentFrameRate": 106.971,
          "historyIdx": 55501
        },
        {
          "valid": true,
          "id": 824174,
          "timestamp": 7375157767,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 107.333,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824174,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375157767
          },
          "type": "frame",
          "currentFrameRate": 107.333,
          "historyIdx": 55502
        },
        {
          "valid": true,
          "id": 824176,
          "timestamp": 7375175837,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 107.625,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824176,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375175837
          },
          "type": "frame",
          "currentFrameRate": 107.625,
          "historyIdx": 55503
        },
        {
          "valid": true,
          "id": 824178,
          "timestamp": 7375193867,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 107.909,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824178,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375193867
          },
          "type": "frame",
          "currentFrameRate": 107.909,
          "historyIdx": 55504
        },
        {
          "valid": true,
          "id": 824180,
          "timestamp": 7375211984,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 108.126,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824180,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375211984
          },
          "type": "frame",
          "currentFrameRate": 108.126,
          "historyIdx": 55505
        },
        {
          "valid": true,
          "id": 824181,
          "timestamp": 7375221005,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 108.247,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824181,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375221005
          },
          "type": "frame",
          "currentFrameRate": 108.247,
          "historyIdx": 55506
        },
        {
          "valid": true,
          "id": 824183,
          "timestamp": 7375239106,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 108.442,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824183,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375239106
          },
          "type": "frame",
          "currentFrameRate": 108.442,
          "historyIdx": 55507
        },
        {
          "valid": true,
          "id": 824185,
          "timestamp": 7375257290,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 108.574,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824185,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375257290
          },
          "type": "frame",
          "currentFrameRate": 108.574,
          "historyIdx": 55508
        },
        {
          "valid": true,
          "id": 824187,
          "timestamp": 7375275242,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 108.817,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824187,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375275242
          },
          "type": "frame",
          "currentFrameRate": 108.817,
          "historyIdx": 55509
        },
        {
          "valid": true,
          "id": 824189,
          "timestamp": 7375293408,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 108.93,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824189,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375293408
          },
          "type": "frame",
          "currentFrameRate": 108.93,
          "historyIdx": 55510
        },
        {
          "valid": true,
          "id": 824191,
          "timestamp": 7375311388,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 109.128,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824191,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375311388
          },
          "type": "frame",
          "currentFrameRate": 109.128,
          "historyIdx": 55511
        },
        {
          "valid": true,
          "id": 824193,
          "timestamp": 7375329509,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 109.237,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824193,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375329509
          },
          "type": "frame",
          "currentFrameRate": 109.237,
          "historyIdx": 55512
        },
        {
          "valid": true,
          "id": 824194,
          "timestamp": 7375338510,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 109.319,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824194,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375338510
          },
          "type": "frame",
          "currentFrameRate": 109.319,
          "historyIdx": 55513
        },
        {
          "valid": true,
          "id": 824196,
          "timestamp": 7375356711,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 109.367,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824196,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375356711
          },
          "type": "frame",
          "currentFrameRate": 109.367,
          "historyIdx": 55514
        },
        {
          "valid": true,
          "id": 824198,
          "timestamp": 7375374786,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 109.474,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824198,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375374786
          },
          "type": "frame",
          "currentFrameRate": 109.474,
          "historyIdx": 55515
        },
        {
          "valid": true,
          "id": 824200,
          "timestamp": 7375392725,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 109.649,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824200,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375392725
          },
          "type": "frame",
          "currentFrameRate": 109.649,
          "historyIdx": 55516
        },
        {
          "valid": true,
          "id": 824202,
          "timestamp": 7375410828,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 109.723,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824202,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375410828
          },
          "type": "frame",
          "currentFrameRate": 109.723,
          "historyIdx": 55517
        },
        {
          "valid": true,
          "id": 824204,
          "timestamp": 7375428981,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 109.76,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824204,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375428981
          },
          "type": "frame",
          "currentFrameRate": 109.76,
          "historyIdx": 55518
        },
        {
          "valid": true,
          "id": 824205,
          "timestamp": 7375438038,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 109.789,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824205,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375438038
          },
          "type": "frame",
          "currentFrameRate": 109.789,
          "historyIdx": 55519
        },
        {
          "valid": true,
          "id": 824207,
          "timestamp": 7375456222,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 109.803,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824207,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375456222
          },
          "type": "frame",
          "currentFrameRate": 109.803,
          "historyIdx": 55520
        },
        {
          "valid": true,
          "id": 824209,
          "timestamp": 7375474123,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 109.966,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824209,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375474123
          },
          "type": "frame",
          "currentFrameRate": 109.966,
          "historyIdx": 55521
        },
        {
          "valid": true,
          "id": 824211,
          "timestamp": 7375492265,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 109.988,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824211,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375492265
          },
          "type": "frame",
          "currentFrameRate": 109.988,
          "historyIdx": 55522
        },
        {
          "valid": true,
          "id": 824213,
          "timestamp": 7375510241,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.098,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824213,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375510241
          },
          "type": "frame",
          "currentFrameRate": 110.098,
          "historyIdx": 55523
        },
        {
          "valid": true,
          "id": 824215,
          "timestamp": 7375528389,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.109,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824215,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375528389
          },
          "type": "frame",
          "currentFrameRate": 110.109,
          "historyIdx": 55524
        },
        {
          "valid": true,
          "id": 824216,
          "timestamp": 7375537411,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.141,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824216,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375537411
          },
          "type": "frame",
          "currentFrameRate": 110.141,
          "historyIdx": 55525
        },
        {
          "valid": true,
          "id": 824218,
          "timestamp": 7375555525,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.162,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824218,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375555525
          },
          "type": "frame",
          "currentFrameRate": 110.162,
          "historyIdx": 55526
        },
        {
          "valid": true,
          "id": 824220,
          "timestamp": 7375573501,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.255,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824220,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375573501
          },
          "type": "frame",
          "currentFrameRate": 110.255,
          "historyIdx": 55527
        },
        {
          "valid": true,
          "id": 824222,
          "timestamp": 7375591709,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.218,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824222,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375591709
          },
          "type": "frame",
          "currentFrameRate": 110.218,
          "historyIdx": 55528
        },
        {
          "valid": true,
          "id": 824224,
          "timestamp": 7375609719,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.287,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824224,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375609719
          },
          "type": "frame",
          "currentFrameRate": 110.287,
          "historyIdx": 55529
        },
        {
          "valid": true,
          "id": 824226,
          "timestamp": 7375627780,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.325,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824226,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375627780
          },
          "type": "frame",
          "currentFrameRate": 110.325,
          "historyIdx": 55530
        },
        {
          "valid": true,
          "id": 824228,
          "timestamp": 7375645976,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.287,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824228,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375645976
          },
          "type": "frame",
          "currentFrameRate": 110.287,
          "historyIdx": 55531
        },
        {
          "valid": true,
          "id": 824230,
          "timestamp": 7375664053,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.314,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824230,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375664053
          },
          "type": "frame",
          "currentFrameRate": 110.314,
          "historyIdx": 55532
        },
        {
          "valid": true,
          "id": 824231,
          "timestamp": 7375673019,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.368,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824231,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375673019
          },
          "type": "frame",
          "currentFrameRate": 110.368,
          "historyIdx": 55533
        },
        {
          "valid": true,
          "id": 824233,
          "timestamp": 7375691135,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.368,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824233,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375691135
          },
          "type": "frame",
          "currentFrameRate": 110.368,
          "historyIdx": 55534
        },
        {
          "valid": true,
          "id": 824235,
          "timestamp": 7375709116,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.443,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824235,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375709116
          },
          "type": "frame",
          "currentFrameRate": 110.443,
          "historyIdx": 55535
        },
        {
          "valid": true,
          "id": 824237,
          "timestamp": 7375727266,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.419,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824237,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375727266
          },
          "type": "frame",
          "currentFrameRate": 110.419,
          "historyIdx": 55536
        },
        {
          "valid": true,
          "id": 824239,
          "timestamp": 7375745239,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.494,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824239,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375745239
          },
          "type": "frame",
          "currentFrameRate": 110.494,
          "historyIdx": 55537
        },
        {
          "valid": true,
          "id": 824241,
          "timestamp": 7375763310,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.511,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824241,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375763310
          },
          "type": "frame",
          "currentFrameRate": 110.511,
          "historyIdx": 55538
        },
        {
          "valid": true,
          "id": 824242,
          "timestamp": 7375772373,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.504,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824242,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375772373
          },
          "type": "frame",
          "currentFrameRate": 110.504,
          "historyIdx": 55539
        },
        {
          "valid": true,
          "id": 824244,
          "timestamp": 7375790467,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.507,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824244,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375790467
          },
          "type": "frame",
          "currentFrameRate": 110.507,
          "historyIdx": 55540
        },
        {
          "valid": true,
          "id": 824246,
          "timestamp": 7375808435,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.577,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824246,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375808435
          },
          "type": "frame",
          "currentFrameRate": 110.577,
          "historyIdx": 55541
        },
        {
          "valid": true,
          "id": 824248,
          "timestamp": 7375826626,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.525,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824248,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375826626
          },
          "type": "frame",
          "currentFrameRate": 110.525,
          "historyIdx": 55542
        },
        {
          "valid": true,
          "id": 824250,
          "timestamp": 7375844802,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.48,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824250,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375844802
          },
          "type": "frame",
          "currentFrameRate": 110.48,
          "historyIdx": 55543
        },
        {
          "valid": true,
          "id": 824252,
          "timestamp": 7375862737,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.569,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824252,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375862737
          },
          "type": "frame",
          "currentFrameRate": 110.569,
          "historyIdx": 55544
        },
        {
          "valid": true,
          "id": 824253,
          "timestamp": 7375871906,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.502,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824253,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375871906
          },
          "type": "frame",
          "currentFrameRate": 110.502,
          "historyIdx": 55545
        },
        {
          "valid": true,
          "id": 824255,
          "timestamp": 7375889844,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.592,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824255,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375889844
          },
          "type": "frame",
          "currentFrameRate": 110.592,
          "historyIdx": 55546
        },
        {
          "valid": true,
          "id": 824257,
          "timestamp": 7375908015,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.546,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824257,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375908015
          },
          "type": "frame",
          "currentFrameRate": 110.546,
          "historyIdx": 55547
        },
        {
          "valid": true,
          "id": 824259,
          "timestamp": 7375925999,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.603,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824259,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375925999
          },
          "type": "frame",
          "currentFrameRate": 110.603,
          "historyIdx": 55548
        },
        {
          "valid": true,
          "id": 824261,
          "timestamp": 7375944078,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.608,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824261,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375944078
          },
          "type": "frame",
          "currentFrameRate": 110.608,
          "historyIdx": 55549
        },
        {
          "valid": true,
          "id": 824263,
          "timestamp": 7375962185,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.594,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824263,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375962185
          },
          "type": "frame",
          "currentFrameRate": 110.594,
          "historyIdx": 55550
        },
        {
          "valid": true,
          "id": 824264,
          "timestamp": 7375971211,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.603,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824264,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375971211
          },
          "type": "frame",
          "currentFrameRate": 110.603,
          "historyIdx": 55551
        },
        {
          "valid": true,
          "id": 824266,
          "timestamp": 7375989391,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.549,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824266,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7375989391
          },
          "type": "frame",
          "currentFrameRate": 110.549,
          "historyIdx": 55552
        },
        {
          "valid": true,
          "id": 824268,
          "timestamp": 7376007369,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.608,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824268,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376007369
          },
          "type": "frame",
          "currentFrameRate": 110.608,
          "historyIdx": 55553
        },
        {
          "valid": true,
          "id": 824270,
          "timestamp": 7376025517,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.571,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824270,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376025517
          },
          "type": "frame",
          "currentFrameRate": 110.571,
          "historyIdx": 55554
        },
        {
          "valid": true,
          "id": 824272,
          "timestamp": 7376043478,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.639,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824272,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376043478
          },
          "type": "frame",
          "currentFrameRate": 110.639,
          "historyIdx": 55555
        },
        {
          "valid": true,
          "id": 824274,
          "timestamp": 7376061638,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.597,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824274,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376061638
          },
          "type": "frame",
          "currentFrameRate": 110.597,
          "historyIdx": 55556
        },
        {
          "valid": true,
          "id": 824275,
          "timestamp": 7376070628,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.625,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824275,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376070628
          },
          "type": "frame",
          "currentFrameRate": 110.625,
          "historyIdx": 55557
        },
        {
          "valid": true,
          "id": 824277,
          "timestamp": 7376106929,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 101.619,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824277,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376106929
          },
          "type": "frame",
          "currentFrameRate": 101.619,
          "historyIdx": 55558
        },
        {
          "valid": true,
          "id": 824279,
          "timestamp": 7376124903,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 102.447,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824279,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376124903
          },
          "type": "frame",
          "currentFrameRate": 102.447,
          "historyIdx": 55559
        },
        {
          "valid": true,
          "id": 824281,
          "timestamp": 7376142942,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 103.175,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824281,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376142942
          },
          "type": "frame",
          "currentFrameRate": 103.175,
          "historyIdx": 55560
        },
        {
          "valid": true,
          "id": 824283,
          "timestamp": 7376160950,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 103.854,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824283,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376160950
          },
          "type": "frame",
          "currentFrameRate": 103.854,
          "historyIdx": 55561
        },
        {
          "valid": true,
          "id": 824285,
          "timestamp": 7376179091,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 104.41,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824285,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376179091
          },
          "type": "frame",
          "currentFrameRate": 104.41,
          "historyIdx": 55562
        },
        {
          "valid": true,
          "id": 824287,
          "timestamp": 7376197212,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 104.927,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824287,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376197212
          },
          "type": "frame",
          "currentFrameRate": 104.927,
          "historyIdx": 55563
        },
        {
          "valid": true,
          "id": 824288,
          "timestamp": 7376206277,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 105.165,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824288,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376206277
          },
          "type": "frame",
          "currentFrameRate": 105.165,
          "historyIdx": 55564
        },
        {
          "valid": true,
          "id": 824290,
          "timestamp": 7376224235,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 105.698,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824290,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376224235
          },
          "type": "frame",
          "currentFrameRate": 105.698,
          "historyIdx": 55565
        },
        {
          "valid": true,
          "id": 824292,
          "timestamp": 7376242312,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 106.128,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824292,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376242312
          },
          "type": "frame",
          "currentFrameRate": 106.128,
          "historyIdx": 55566
        },
        {
          "valid": true,
          "id": 824294,
          "timestamp": 7376260440,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 106.491,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824294,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376260440
          },
          "type": "frame",
          "currentFrameRate": 106.491,
          "historyIdx": 55567
        },
        {
          "valid": true,
          "id": 824296,
          "timestamp": 7376278440,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 106.889,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824296,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376278440
          },
          "type": "frame",
          "currentFrameRate": 106.889,
          "historyIdx": 55568
        },
        {
          "valid": true,
          "id": 824298,
          "timestamp": 7376296589,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 107.178,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824298,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376296589
          },
          "type": "frame",
          "currentFrameRate": 107.178,
          "historyIdx": 55569
        },
        {
          "valid": true,
          "id": 824299,
          "timestamp": 7376305593,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 107.349,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824299,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376305593
          },
          "type": "frame",
          "currentFrameRate": 107.349,
          "historyIdx": 55570
        },
        {
          "valid": true,
          "id": 824301,
          "timestamp": 7376323715,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 107.612,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824301,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376323715
          },
          "type": "frame",
          "currentFrameRate": 107.612,
          "historyIdx": 55571
        },
        {
          "valid": true,
          "id": 824303,
          "timestamp": 7376341745,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 107.897,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824303,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376341745
          },
          "type": "frame",
          "currentFrameRate": 107.897,
          "historyIdx": 55572
        },
        {
          "valid": true,
          "id": 824305,
          "timestamp": 7376359877,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 108.107,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824305,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376359877
          },
          "type": "frame",
          "currentFrameRate": 108.107,
          "historyIdx": 55573
        },
        {
          "valid": true,
          "id": 824307,
          "timestamp": 7376377980,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 108.314,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824307,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376377980
          },
          "type": "frame",
          "currentFrameRate": 108.314,
          "historyIdx": 55574
        },
        {
          "valid": true,
          "id": 824309,
          "timestamp": 7376395995,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 108.546,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824309,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376395995
          },
          "type": "frame",
          "currentFrameRate": 108.546,
          "historyIdx": 55575
        },
        {
          "valid": true,
          "id": 824310,
          "timestamp": 7376405089,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 108.609,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824310,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376405089
          },
          "type": "frame",
          "currentFrameRate": 108.609,
          "historyIdx": 55576
        },
        {
          "valid": true,
          "id": 824312,
          "timestamp": 7376423130,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 108.803,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824312,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376423130
          },
          "type": "frame",
          "currentFrameRate": 108.803,
          "historyIdx": 55577
        },
        {
          "valid": true,
          "id": 824314,
          "timestamp": 7376441251,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 108.939,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824314,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376441251
          },
          "type": "frame",
          "currentFrameRate": 108.939,
          "historyIdx": 55578
        },
        {
          "valid": true,
          "id": 824316,
          "timestamp": 7376459184,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 109.161,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824316,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376459184
          },
          "type": "frame",
          "currentFrameRate": 109.161,
          "historyIdx": 55579
        },
        {
          "valid": true,
          "id": 824318,
          "timestamp": 7376477353,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 109.243,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824318,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376477353
          },
          "type": "frame",
          "currentFrameRate": 109.243,
          "historyIdx": 55580
        },
        {
          "valid": true,
          "id": 824320,
          "timestamp": 7376495502,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 109.325,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824320,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376495502
          },
          "type": "frame",
          "currentFrameRate": 109.325,
          "historyIdx": 55581
        },
        {
          "valid": true,
          "id": 824322,
          "timestamp": 7376513471,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 109.495,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824322,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376513471
          },
          "type": "frame",
          "currentFrameRate": 109.495,
          "historyIdx": 55582
        },
        {
          "valid": true,
          "id": 824323,
          "timestamp": 7376522622,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 109.485,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824323,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376522622
          },
          "type": "frame",
          "currentFrameRate": 109.485,
          "historyIdx": 55583
        },
        {
          "valid": true,
          "id": 824325,
          "timestamp": 7376540606,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 109.632,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824325,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376540606
          },
          "type": "frame",
          "currentFrameRate": 109.632,
          "historyIdx": 55584
        },
        {
          "valid": true,
          "id": 824327,
          "timestamp": 7376558693,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 109.717,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824327,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376558693
          },
          "type": "frame",
          "currentFrameRate": 109.717,
          "historyIdx": 55585
        },
        {
          "valid": true,
          "id": 824329,
          "timestamp": 7376576739,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 109.813,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824329,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376576739
          },
          "type": "frame",
          "currentFrameRate": 109.813,
          "historyIdx": 55586
        },
        {
          "valid": true,
          "id": 824331,
          "timestamp": 7376594862,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 109.862,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824331,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376594862
          },
          "type": "frame",
          "currentFrameRate": 109.862,
          "historyIdx": 55587
        },
        {
          "valid": true,
          "id": 824333,
          "timestamp": 7376612964,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 109.918,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824333,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376612964
          },
          "type": "frame",
          "currentFrameRate": 109.918,
          "historyIdx": 55588
        },
        {
          "valid": true,
          "id": 824334,
          "timestamp": 7376622044,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 109.927,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824334,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376622044
          },
          "type": "frame",
          "currentFrameRate": 109.927,
          "historyIdx": 55589
        },
        {
          "valid": true,
          "id": 824336,
          "timestamp": 7376640118,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 109.987,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824336,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376640118
          },
          "type": "frame",
          "currentFrameRate": 109.987,
          "historyIdx": 55590
        },
        {
          "valid": true,
          "id": 824338,
          "timestamp": 7376658097,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.096,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824338,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376658097
          },
          "type": "frame",
          "currentFrameRate": 110.096,
          "historyIdx": 55591
        },
        {
          "valid": true,
          "id": 824340,
          "timestamp": 7376676243,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.107,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824340,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376676243
          },
          "type": "frame",
          "currentFrameRate": 110.107,
          "historyIdx": 55592
        },
        {
          "valid": true,
          "id": 824342,
          "timestamp": 7376694219,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.206,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824342,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376694219
          },
          "type": "frame",
          "currentFrameRate": 110.206,
          "historyIdx": 55593
        },
        {
          "valid": true,
          "id": 824344,
          "timestamp": 7376712344,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.22,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824344,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376712344
          },
          "type": "frame",
          "currentFrameRate": 110.22,
          "historyIdx": 55594
        },
        {
          "valid": true,
          "id": 824346,
          "timestamp": 7376730492,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.217,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824346,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376730492
          },
          "type": "frame",
          "currentFrameRate": 110.217,
          "historyIdx": 55595
        },
        {
          "valid": true,
          "id": 824347,
          "timestamp": 7376739505,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.25,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824347,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376739505
          },
          "type": "frame",
          "currentFrameRate": 110.25,
          "historyIdx": 55596
        },
        {
          "valid": true,
          "id": 824349,
          "timestamp": 7376757626,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.258,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824349,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376757626
          },
          "type": "frame",
          "currentFrameRate": 110.258,
          "historyIdx": 55597
        },
        {
          "valid": true,
          "id": 824351,
          "timestamp": 7376775555,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.368,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824351,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376775555
          },
          "type": "frame",
          "currentFrameRate": 110.368,
          "historyIdx": 55598
        },
        {
          "valid": true,
          "id": 824353,
          "timestamp": 7376793751,
          "hands": [],
          "handsMap": {},
          "pointables": [],
          "tools": [],
          "fingers": [],
          "interactionBox": {
            "valid": true,
            "center": [
              0,
              200,
              0
            ],
            "size": [
              235.247,
              235.247,
              147.751
            ],
            "width": 235.247,
            "height": 235.247,
            "depth": 147.751
          },
          "gestures": [],
          "pointablesMap": {},
          "_translation": [
            105.421,
            -926.319,
            917.042
          ],
          "_rotation": [
            -0.617789,
            -0.588739,
            0.521271,
            0.0917767,
            -0.712358,
            -0.695789,
            0.78097,
            -0.38201,
            0.49412
          ],
          "_scaleFactor": -1.18989,
          "data": {
            "currentFrameRate": 110.331,
            "devices": [],
            "gestures": [],
            "hands": [],
            "id": 824353,
            "interactionBox": {},
            "pointables": [],
            "r": [
              [
                -0.617789,
                -0.588739,
                0.521271
              ],
              [
                0.0917767,
                -0.712358,
                -0.695789
              ],
              [
                0.78097,
                -0.38201,
                0.49412
              ]
            ],
            "s": -1.18989,
            "timestamp": 7376793751
          },
          "type": "frame",
          "currentFrameRate": 110.331,
          "historyIdx": 55599
        }
      ],
      "size": 200
    },
    "lastConnectionFrame": {
      "valid": true,
      "id": 824369,
      "timestamp": 7376938324,
      "hands": [],
      "handsMap": {},
      "pointables": [],
      "tools": [],
      "fingers": [],
      "interactionBox": {
        "valid": true,
        "center": [
          0,
          200,
          0
        ],
        "size": [
          235.247,
          235.247,
          147.751
        ],
        "width": 235.247,
        "height": 235.247,
        "depth": 147.751
      },
      "gestures": [],
      "pointablesMap": {},
      "_translation": [
        105.421,
        -926.319,
        917.042
      ],
      "_rotation": [
        -0.617789,
        -0.588739,
        0.521271,
        0.0917767,
        -0.712358,
        -0.695789,
        0.78097,
        -0.38201,
        0.49412
      ],
      "_scaleFactor": -1.18989,
      "data": {
        "currentFrameRate": 110.5,
        "devices": [],
        "gestures": [],
        "hands": [],
        "id": 824369,
        "interactionBox": {},
        "pointables": [],
        "r": [
          [
            -0.617789,
            -0.588739,
            0.521271
          ],
          [
            0.0917767,
            -0.712358,
            -0.695789
          ],
          [
            0.78097,
            -0.38201,
            0.49412
          ]
        ],
        "s": -1.18989,
        "timestamp": 7376938324
      },
      "type": "frame",
      "currentFrameRate": 110.5
    },
    "accumulatedGestures": [],
    "checkVersion": true,
    "connection": {
      "opts": {
        "frameEventName": "animationFrame",
        "suppressAnimationLoop": false,
        "loopWhileDisconnected": true,
        "useAllPlugins": false,
        "checkVersion": true,
        "host": "127.0.0.1",
        "enableGestures": false,
        "scheme": "ws:",
        "port": 6437,
        "background": false,
        "optimizeHMD": false,
        "requestProtocolVersion": 6
      },
      "host": "127.0.0.1",
      "port": 6437,
      "scheme": "ws:",
      "protocolVersionVerified": true,
      "background": false,
      "optimizeHMD": false,
      "_events": {
        "ready": [
          null,
          null,
          null
        ],
        "disconnect": [
          null,
          null,
          null
        ],
        "frame": [
          null,
          null
        ]
      },
      "socket": {},
      "connected": true,
      "gesturesEnabled": false,
      "windowVisible": false,
      "focusedState": false,
      "focusDetectorTimer": 12
    },
    "streamingCount": 1,
    "devices": {
      "S323A092986": {
        "attached": true,
        "id": "S323A092986",
        "streaming": true,
        "type": "Peripheral"
      }
    },
    "plugins": {},
    "_pluginPipelineSteps": {},
    "_pluginExtendedMethods": {},
    "_events": {
      "animationFrame": [
        null,
        null
      ]
    }
  },
  "historyIdx": 809
}
