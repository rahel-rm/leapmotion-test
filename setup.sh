#!/bin/bash


curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.1/install.sh | bash

sudo dnf install git
git config --global user.email "rahelini@gmail.com"
git config --global user.name "Rahel Rjadnev-Meristo"


nvm install stable && nvm use stable
npm -g install yarn
yarn && yarn dev
